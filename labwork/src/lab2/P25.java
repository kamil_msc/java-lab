package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P25 {
    public static void main(String args[]){
        int odd = 0,even = 0, zero = 0;
        try{
            for(int i = 0;i<args.length;i++) {
                int x = Integer.parseInt(args[i]);
                if (x % 2 == 1) odd++;
                else if (x == 0) zero++;
                else even++;
            }
            System.out.println("Odd " + odd + "\nEven " + even + "\nZero " + zero);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
