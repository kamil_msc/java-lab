package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P16 {
    public static void main(String args[]){
        int no_ints = 0, no_of_double= 0,no_of_longs = 0,no_of_stg = 0;
        for(int i = 0;i<args.length;i++){
            try{
                Integer.valueOf(args[i]);
                no_ints++;
            }catch(Exception e){

            }
            try{
                Double.valueOf(args[i]);
                no_of_double++;
            }catch(Exception e){
                no_of_stg++;
            }
            try{
                Long.valueOf(args[i]);
                no_of_longs++;
            }catch(Exception e){

            }
        }
        System.out.println("No of Integers = " + no_ints);
        System.out.println("No of Double = " + no_of_double);
        System.out.println("No of Long = " + no_of_longs);
        System.out.println("No of String = " + no_of_stg);
    }
}
