package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P7 {
    public static void main(String args[]){
        System.out.println("Outer Block Start");
        {
            System.out.println("Inner Block 1");
        }
        {
            System.out.println("Inner Block 2");
        }
        System.out.println("Outer Block End");
    }
}
