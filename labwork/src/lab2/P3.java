package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P3 {
    public static void main(String args[]){
        try{
            int x = Integer.parseInt(args[0]);
            System.out.println(x);
        }catch(Exception e){
            System.out.println("Input " + args[0] + " is not a integer");
        }
    }
}
