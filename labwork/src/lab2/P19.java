package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P19 {
    public static void main(String args[]){
        for(int i = 1;i<=50;i++){
            if(i % 2 == 1) System.out.println(i + " is odd");
            else System.out.println(i + " is even");
        }
    }
}
