package lab2;

/**
 * Created by kkhan on 29/07/20.
 */
public class P24 {
    public static void main(String args[]){
        int x = Integer.valueOf(args[0]);
        if(x > 0) System.out.println("Positive");
        if(x == 0) System.out.println("Zero");
        if(x < 0)System.out.println("Negative");
    }
}
