package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

/**
 * Created by kkhan on 19/08/20.
 */
public class Input {

    public static int getIntInput(String stg){
        System.out.print(stg + "  ");
        try{
            return Integer.valueOf(getInput(stg));
        }catch(Exception e){
            System.out.println("Not a correct input...");
        }
        return -1;
    }



    public static String getInput(String stg){
        System.out.print(stg + "...");
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String s = reader.readLine();
            return s;
        }catch(Exception e){

        }
        return "";
    }
}
