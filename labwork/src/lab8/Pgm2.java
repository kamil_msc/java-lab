package lab8;

import util.Input;

import java.util.*;

/**
 * Created by kkhan on 09/09/20.
 */
public class Pgm2 {

    // Google Suggestion
    static Map dictionary = new HashMap();
    static{
        dictionary.put("table","object to sit");
        dictionary.put("answer","reply to a question");
    }

    public static int diff(String a,String b){
        int len = a.length();
        if(b.length() < len){
            len = b.length();
        }
        int abs_diff = 0;
        for(int i = 0;i<len;i++){
            abs_diff = abs_diff + Math.abs(a.charAt(i) - b.charAt(i));
        }
        return abs_diff;
    }

    public static void main(String args[]){
        while(true){
            String token = Input.getInput("Enter the String to Search. Type to exit to get out \n");
            if(token.equals("exit")){
                break;
            }
            if(dictionary.containsKey(token)){
                System.out.println("Meaning is " + dictionary.get(token));
            }else{
                Iterator<String> iterator = dictionary.keySet().iterator();
                while(iterator.hasNext()){
                    String key = iterator.next();
                    if(diff(token,key) < 10){
                        System.out.println("Do you mean " + key + " and the meaning is " + dictionary.get(key));
                    }
                }
            }
        }
    }

}
