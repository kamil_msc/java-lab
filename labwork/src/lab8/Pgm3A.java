package lab8;

import java.util.Locale;

/**
 * Created by kkhan on 09/09/20.
 */
// Case Related


public class Pgm3A {


    public static void main(String args[]){
        String name = "Central University of Tamil Nadu. Tiruvarur";
        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());

        // You can check I is printed in different unicode in Turkish
        System.out.println("TITLE".toLowerCase(new Locale("tr","TR")));

    }
}
