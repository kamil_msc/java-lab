package lab8;

import java.util.Locale;

/**
 * Created by kkhan on 09/09/20.
 */
// Replace 13,14, Intern 18, valueOf 28
public class Pgm3D {

    public static void main(String args[]){
        System.out.println("trichirapalli".replace('r','z'));
        System.out.println("trichirapalli".replace("tri","abc"));

        // S1 refers to Object in the Heap Area
        String s1 = new String("GFG"); // Line-1
        // S2 refers to Object in SCP Area
        String s2 = s1.intern(); // Line-2
        // Comparing memory locations
        // s1 is in Heap
        // s2 is in SCP
        System.out.println(s1 == s2);
        // Comparing only values
        System.out.println(s1.equals(s2));
        // S3 refers to Object in the SCP Area
        String s3 = "GFG"; // Line-3
        System.out.println(s2 == s3);

        // will give 1231
        System.out.println(String.valueOf(123)+1);
        System.out.println(String.valueOf(false)+1);

        String f = "Roll No: %d, Name : %s";
        int[] roll = new int[]{1,2,3};
        String[] names = new String[]{"Afari Jesse","Aleena Prakash","Arun Kumar"};
        for(int i = 0;i<roll.length;i++)
            System.out.println(String.format(f,roll[i],names[i]));

        // In US decimal is represented as . where as in Germany it is ,
        System.out.format(Locale.US, "%1.2f\n", Math.PI);
        System.out.format(Locale.GERMANY, "%1.2f\n", Math.PI);


    }
}
