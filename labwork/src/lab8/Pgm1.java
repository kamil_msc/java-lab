package lab8;
import java.io.Console;
import util.*;
/**
 * Created by kkhan on 09/09/20.
 */
public class Pgm1 {

    // Google Suggestion
    static String[] words = new String[]{
            "Central Bank of India",
            "Central University of Tamil Nadu",
            "Central University of Rajasthan",
            "Central University of Punjab",
            "Central University of Bihar",
            "Central Reserve Police Force",
            "Central Government"
    };


    public static void main(String args[]){
        while(true){
            String token = Input.getInput("Enter the String to Search. Type to exit to get out \n");
            if(token.equals("exit")){
                break;
            }
            for(String word : words){
                if(word.indexOf(token) != -1){
                    System.out.println(word);
                }

            }
        }
    }
}
