package lab8;

import java.util.*;

/**
 * Created by kkhan on 09/09/20.
 */
// Join, Concat and Split
// 12 - Concat 16 - Split 17 - Split with Limit 9 - join with iterable, 8 - join with variable arguments
public class Pgm3B {

    public static void main(String args[]) {
        String b = "Indian Oil ";
        b = b.concat("Corporation");

        System.out.println(b);

        String s = "Central University of Tamil Nadu";
        String[] tokens = s.split(" ");
        for(int i = 0;i<tokens.length;i++) {
            System.out.println(tokens[i]);
        }
        // Max no of tokens = 2
        tokens = s.split(" ",2);
        for(int i = 0;i<tokens.length;i++) {
            System.out.println(tokens[i]);
        }

        // Join
        List<String> cities = new ArrayList<String>();
        cities.add("Delhi");cities.add("Mumbai");cities.add("Chennai");cities.add("Kolkata");
        String x = String.join(",",cities);
        System.out.println(x);

        //Join with variable arguments
        System.out.println(String.join(",","a","b","c"));
        System.out.println(String.join(",","a","b","c","d"));

    }
}
