package lab8;

/**
 * Created by kkhan on 09/09/20.
 */
// Index functions 19,20,21,22
// CharAt 1, Length 2, isEmpty 11, equals 10, equalsIgnoreCase 15
public class Pgm3C {
    public static void main(String args[]) {
        String name = "tiruchirapalli";
        System.out.println("pos of r in " + name + "  " + name.indexOf("r"));
        System.out.println("pos of z in " + name + "  " + name.indexOf("z"));
        System.out.println("pos of r after 3rd character in " + name + "  " + name.indexOf("r",3));

        System.out.println("pos of ira in " + name + "  " +name.indexOf("ira"));
        System.out.println("pos of irn in " + name + "  " + name.indexOf("irn"));
        System.out.println("pos of ir after 5th character in " + name + "  " + name.indexOf("ir",5));


        System.out.println("Char at 2nd position " + name.charAt(2));
        System.out.println("Length of " + name + " " + name.length());

        System.out.println("is  " + name + " empty "  + name.isEmpty());
        System.out.println("is  " + "" + " empty "  + "".isEmpty());

        System.out.println("Checking Trichy and TRICHY are same irrespective of case " + "Trichy".equalsIgnoreCase("TRICHY"));
        System.out.println("Checking Trichy and TRICHY are same " + "Trichy".equals("TRICHY"));

    }
}
