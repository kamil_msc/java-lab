package lab13;

/**
 * Created by kkhan on 14/10/20.
 */

class PrimeGeneratingThread extends Thread{
    int s,e;

    private boolean isPrime(int no){
        for(int i = 2;i<=no/2;i++){
            if(no % i == 0) return false;
        }
        return true;
    }

    public PrimeGeneratingThread(int s,int e){
        this.s = s;
        this.e = e;
    }

    @Override
    public void run() {
        for(int i = s;i<=e;i++){
            if(isPrime(i)){
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }
    }

}

public class Q2_Thread {

    public static void main(String args[]){
        Thread t1 = new PrimeGeneratingThread(1,1000);
        Thread t2 = new PrimeGeneratingThread(1000,2000);
        t1.start();
        t2.start();

    }

}
