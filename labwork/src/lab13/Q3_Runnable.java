package lab13;

/**
 * Created by kkhan on 14/10/20.
 */

class PrimeGenerator implements Runnable{
    int s,e;

    private boolean isPrime(int no){
        for(int i = 2;i<=no/2;i++){
            if(no % i == 0) return false;
        }
        return true;
    }

    public PrimeGenerator(int s,int e){
        this.s = s;
        this.e = e;
    }

    @Override
    public void run() {
        for(int i = s;i<=e;i++){
            if(isPrime(i)){
                  System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }
    }

}

public class Q3_Runnable {

    public static void main(String args[]){
        Thread t1 = new Thread(new PrimeGenerator(1,1000),"T1");
        Thread t2 = new Thread(new PrimeGenerator(1000,2000),"T2");
        t1.start();
        t2.start();

    }

}
