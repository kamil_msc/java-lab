package lab14;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 * Created by kkhan on 28/10/20.
 */
public class P4 extends Frame {

    public P4() {
        setSize(500, 500);
        setLayout(new FlowLayout());

        add(new Label("Flow Layout Example"));
        for(int i = 0;i<10;i++){
            add(new Button("Button " + i));
        }

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });


    }
    public static void main(String args[]){
        P4 p4 = new P4();
        p4.show();
    }
}
