package lab14;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 * Created by kkhan on 28/10/20.
 */
public class P5 extends Frame {

    public P5() {
        setSize(500, 500);
        setLayout(new BorderLayout());

        add("East",new Label("East"));
        add("West",new Label("West"));
        add("North",new Label("North"));
        add("South",new Label("South"));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });


    }
    public static void main(String args[]){
        P5 p5 = new P5();
        p5.show();
    }
}
