package lab11;

class Test extends Exception { }

class Q32 {
    public static void main(String args[]) {
        try {
            throw new Test();
        }
        catch(Test t) {
            System.out.println("Got the TestException");

        }
        finally {
            System.out.println("Inside finally block ");

        }
    }
}