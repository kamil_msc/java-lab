package lab11;

/**
 * Created by kkhan on 30/09/20.
 */
public class Karnataka extends State {
    @Override
    public String getName() {
        return "Karnataka";
    }

    @Override
    public int getTotalPopulation() {
        return 640000000;
    }

    public Karnataka(){
        simulate(150,4000,3700);
    }
}
