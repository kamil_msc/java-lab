package lab11;

/**
 * Created by kkhan on 30/09/20.
 */
public class Kerala extends State {


    @Override
    public String getName() {
        return "Kerala";
    }

    @Override
    public int getTotalPopulation() {
        return 340000000;
    }

    public Kerala(){
        simulate(50,3000,2900);
    }
}
