package lab11;
import java.util.Calendar;

/**
 * Created by kkhan on 30/09/20.
 */
public interface DailyData {

    public int getAffectedPersonCount(Calendar date);
    public int getCuredPersonCount(Calendar date);
    public int getDeathCount(Calendar date);
}
