package lab11;
import lab11.course.*;
import lab11.course.lab.*;

/**
 * Created by kkhan on 30/09/20.
 */
public class Q1 {

    DataScience ds = new DataScience();
    AdvancedProgramming ap = new AdvancedProgramming();

    private void markAttendence(){
        ds.attend("Vessesh",1,1);
        ds.attend("Beullah",1,new int[]{1,2,3});
        ap.attend("Dayanand",1,new int[]{1,2});
        ap.attend("Vessesh",1,new int[2]);

        ds.print();
        ap.print();
    }

    public static void main(String args[]){
        new Q1().markAttendence();
    }
}
