package lab11;

import java.util.*;

/**
 * Created by kkhan on 30/09/20.
 */
public abstract class State implements DailyData {
    public abstract String getName();

    public abstract int getTotalPopulation();

    public int getTotalCured() {
        Calendar c = getZeroHourCalendar();
        int t = 0;
        for (int i = 1; i < n; i++) {
            c.set(Calendar.DATE, 1);
            t += getCuredPersonCount(c);
        }
        return t;
    }

    public int getTotalAffected() {
        Calendar c = getZeroHourCalendar();
        int t = 0;
        for (int i = 1; i < n; i++) {
            c.set(Calendar.DATE, 1);
            t += getAffectedPersonCount(c);
        }
        return t;
    }

    public int getTotalDeath() {
        Calendar c = getZeroHourCalendar();
        int t = 0;
        for (int i = 1; i < n; i++) {
            c.set(Calendar.DATE, 1);
            t += getDeathCount(c);
        }
        return t;
    }

    protected Map<Long, Integer> affectedPersonCounter = new HashMap<Long, Integer>();

    @Override
    public int getAffectedPersonCount(Calendar date) {
        return affectedPersonCounter.get(date.getTimeInMillis());
    }

    protected Map<Long, Integer> curedPersonCounter = new HashMap<Long, Integer>();

    @Override
    public int getCuredPersonCount(Calendar date) {
        return curedPersonCounter.get(date.getTimeInMillis());
    }

    protected Map<Long, Integer> deathPersonCounter = new HashMap<Long, Integer>();

    @Override
    public int getDeathCount(Calendar date) {
        return deathPersonCounter.get(date.getTimeInMillis());
    }

    private Calendar getZeroHourCalendar(){
        Calendar c = new GregorianCalendar();
        c.set(Calendar.MONTH, 8);
        c.set(Calendar.HOUR_OF_DAY,23);
        c.set(Calendar.MINUTE,59);
        c.set(Calendar.SECOND,59);
        c.set(Calendar.MILLISECOND,999);
        return c;
    }

    int n = 31;
    protected void simulate(int b1, int b2, int b3) {
        Calendar c = getZeroHourCalendar();
        Random rand = new Random();
        for (int i = 1; i < n; i++) {
            c.set(Calendar.DATE, i);
            deathPersonCounter.put(c.getTimeInMillis(), rand.nextInt(b1));
            affectedPersonCounter.put(c.getTimeInMillis(), rand.nextInt(b2));
            curedPersonCounter.put(c.getTimeInMillis(), rand.nextInt(b3));
        }

    }

    protected void predict(){
        System.out.println("State " + getName());
        int affected_prediction = getTotalAffected()/n;
        int death_prediction = getTotalDeath()/n;
        int cured_prediction = getTotalCured()/n;
        System.out.println("Collected Data...");
        Calendar c = getZeroHourCalendar();
        for(int i = 1;i<n;i++){
            c.set(Calendar.DATE,i);
            System.out.println(c.getTime() + " A " + getAffectedPersonCount(c) + " C " + getCuredPersonCount(c) + " D " + getDeathCount(c));

        }
        c.add(Calendar.DATE,1);
        double d = getTotalAffected() / getTotalPopulation();
        d = d * 100;
        System.out.println("Prediction.....");
        System.out.println(c.getTime() + " A " + affected_prediction + " C " + cured_prediction + " D " + death_prediction);
        System.out.println("Percentage of Population affected " + d);

    }

}
