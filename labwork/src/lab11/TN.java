package lab11;

import java.util.*;

/**
 * Created by kkhan on 30/09/20.
 */
public class TN extends State {

    @Override
    public String getName() {
        return "TAMIL NADU";
    }

    @Override
    public int getTotalPopulation() {
        return 690000000;
    }

    public TN(){
        simulate(100,6000,5900);
    }



}
