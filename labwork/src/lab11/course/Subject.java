package lab11.course;

import java.util.*;

/**
 *
 * Created by kkhan on 30/09/20.
 */
public abstract class Subject {
    protected Map<Integer,List> attendence = new HashMap<Integer,List>();
    public abstract String getName();
    public abstract int noOfClasses();

    private void init(int week){
        if(!attendence.containsKey(week)){
            List xList = new ArrayList<>();
            for(int i = 0;i<noOfClasses();i++){
                xList.add(new ArrayList<String>());
            }
            attendence.put(week,xList);
        }
    }
    public void attend(String student,int week, int class_no){
        init(week);
        List<List> week_list = attendence.get(week);
        week_list.get(class_no).add(student);
    }

    public void attend(String student,int week, int[] class_nos){
        init(week);
        if(!attendence.containsKey(week)){
            List xList = new ArrayList<>();
            for(int i = 0;i<noOfClasses();i++){
                xList.add(new ArrayList<String>());
            }
            attendence.put(week,xList);
        }
        List<List> week_list = attendence.get(week);
        for(int i = 0;i<class_nos.length;i++)
            week_list.get(class_nos[i]).add(student);
    }

    public void print(){
        System.out.println("Subject Name:" + getName() + " No of classes per week " + noOfClasses());
        Iterator<Integer> weekIterator = attendence.keySet().iterator();
        while(weekIterator.hasNext()){
            Integer weekKey = weekIterator.next();
            System.out.println("Week : " + weekKey);
            List<List> lists = attendence.get(weekKey);
            for(int i = 0;i<lists.size();i++){
                System.out.println("Class No " + (i+1) + " No of Students Attended " + lists.get(i).size());

            }
        }
    }
}

