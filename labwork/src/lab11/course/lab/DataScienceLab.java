package lab11.course.lab;
import lab11.course.*;

/**
 * Created by kkhan on 30/09/20.
 */
public class DataScienceLab extends  Subject {
    @Override
    public String getName() {
        return "Data Science Lab";
    }

    @Override
    public int noOfClasses() {
        return 1;
    }
}
