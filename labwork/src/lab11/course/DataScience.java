package lab11.course;

/**
 * Created by kkhan on 30/09/20.
 */
public class DataScience extends Subject {

    @Override
    public String getName() {
        return "DataScience";
    }

    @Override
    public int noOfClasses() {
        return 4;
    }
}
