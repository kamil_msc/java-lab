package lab11.course;

/**
 * Created by kkhan on 30/09/20.
 */
public class AdvancedProgramming extends  Subject {

    @Override
    public String getName() {
        return "Advanced Programming";
    }

    @Override
    public int noOfClasses() {
        return 3;
    }
}
