package package1;

/**
 * Created by kkhan on 24/09/20.
 */
class Derived extends PublicVisibility{
    public Derived(){
        System.out.println(protected_variable);
        System.out.println(public_variable);
        System.out.println(default_variable);
        // private variable is not visible. If uncommented next line will show compilation errors.
        //System.out.println(private_variable);
    }
}

public class InsidePackageAccess {
    PublicVisibility publicVisibility = new PublicVisibility();
    // Default visibility class is not accessible across packages. Next line if uncommented will show compilation errors
    //DefaultVisibility dv = new DefaultVisibility();

    public InsidePackageAccess(){
        // public visibility variable is visible
        System.out.println(publicVisibility.public_variable);
        //Default Visibility variable is  visible.
        System.out.println(publicVisibility.default_variable);
        // protected Visibility variable is  visible.
        System.out.println(publicVisibility.protected_variable);
        // Private variable can't be accessed. uncommenting following line will throw compilation errors
        //System.out.println(publicVisibility.private_variable);

    }
}