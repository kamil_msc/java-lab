package package1;

/**
 * Created by kkhan on 24/09/20.
 */
public class PublicVisibility {

    private int private_variable;
    public int public_variable = 12;
    int default_variable;
    protected int protected_variable = 23;
}
