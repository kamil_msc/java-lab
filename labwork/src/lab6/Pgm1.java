package lab6;

/**
 * Created by kkhan on 26/08/20.
 */
public class Pgm1 {

    public int findDuplicatePosition(int[] array,int value,int start,int end){
        for(int i = start+1;i<end;i++){
            if(array[i] == value){
                return i;
            }
        }
        return -1;
    }

    public int[] removeDuplicates(int[] a){
        print(a,a.length);
        int start = 0;
        int end = a.length;
        while(start < end){
            int value = a[start];
            int dup_pos = 0;
            do {
                dup_pos = findDuplicatePosition(a, value, start, end);
                if(dup_pos != -1) {
                    for (int i = dup_pos; i < end-1; i++) {
                        a[i] = a[i + 1];
                    }
                    a[end-1] = value;
                    end = end - 1;
                    //print(a,end);
                }
            }
            while(dup_pos != -1);
            start = start + 1;
        }
        print(a,end);
        System.out.println("------------");
        return a;
    }

    private void print(int[] array,int end){
        for(int i = 0;i<end;i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String args[]) {
        Pgm1 p1 = new Pgm1();
        p1.removeDuplicates(new int[]{12, 24, 35, 24, 88, 120, 155, 88, 120, 155, 155, 80, 155});
        p1.removeDuplicates(new int[]{4,3, 3, 2, 2, 0, 7, 6, 6, 3, 9, 8, 3, 0, 1, 9, 0, 4, 8, 7, 0, 7, 5, 8, 9, 4, 4, 7, 6, 4, 7, 2, 6, 9, 1, 0, 5, 9,2, 3, 3, 2, 6,
                8, 8, 2, 2, 3, 0, 3, 5, 3, 1, 3, 1, 1, 8, 5, 2, 7, 9, 0, 3, 0, 4, 5, 7, 4, 8, 0, 7, 0, 0, 1, 4, 0, 8, 7, 1, 7, 0, 1, 6, 5, 5, 7,
                1, 3, 8, 3, 0, 9, 9});
        p1.removeDuplicates(new int[]{1,8, 9, 6, 0, 3, 2, 1, 0, 6, 2, 0, 4, 1, 0, 3, 7, 1, 6, 5, 3, 9, 9, 5, 5, 0, 9, 9, 1, 1, 3, 8, 2, 3, 4, 7, 5, 2, 1, 1, 5, 0, 4,
                3, 9, 3, 0, 3, 9, 4, 9, 8, 2, 2, 2, 2, 2,});
        p1.removeDuplicates(new int[]{3, 2, 8, 8, 0, 4, 6, 4, 5, 7, 0, 0, 0, 7, 2, 7, 4, 1, 7, 0, 6, 3, 1, 8, 7, 7, 5, 9, 9, 7, 7, 4, 4, 4, 5, 6, 6, 6, 3, 3, 2, 5, 3,
                1, 3, 6, 8, 0, 1, 5, 2, 5, 0, 1, 4, 2, 6, 5, 7, 9, 2, 2, 5, 4, 0, 8, 6, 4, 9, 4, 9, 0, 8, 4, 3, 7, 5, 0, 1, 4, 3, 7, 8, 0, 7, 3,
                3, 3, 8, 6, 9, 4, 4, 6, 6, 2, 2, 5, 0, 9, 3, 3, 7, 9, 9, 1, 0, 6, 9, 9, 3, 3, 7, 5, 3, 4, 2, 0, 3, 0, 1, 7, 6, 8, 8, 5, 5,});
        p1.removeDuplicates(new int[]{1, 1, 9, 5, 2, 1, 4, 1, 1, 4, 0, 6, 1, 4, 3, 3, 2, 2, 0});
        p1.removeDuplicates(new int[]{8, 0, 4, 5, 4, 2, 6, 1, 7, 1, 4, 8, 8, 5, 8, 0, 2, 2, 5, 7});
        p1.removeDuplicates(new int[]{1,1, 9, 5, 2, 1, 4, 1, 1, 4, 0, 6, 1, 4, 3, 3, 2, 2, 0, 7, 6, 6, 3, 9, 8, 3, 0, 1, 9, 0, 4, 8, 7, 0, 7, 5, 8, 9, 4, 4, 7, 6, 4,
                7, 2, 6, 9, 1, 0, 5, 9, 2, 3, 3, 2, 6, 8, 8, 2, 2, 3});



    }
}
