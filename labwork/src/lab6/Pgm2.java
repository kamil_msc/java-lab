package lab6;

/**
 * Created by kkhan on 26/08/20.
 */
public class Pgm2 {

    public void print(int[][] array){
        for(int i = 0;i<array.length;i++){
            for(int j = 0;j<array[i].length;j++){
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void lower_triangular_matrix(int a[][]){
        print(a);
        for(int i =0;i<a.length;i++){
            for(int j = 0;j<a[i].length;j++){
                if(i < j) a[i][j] = 0;
            }
        }
        System.out.println("Lower Triangular Matrix ");
        print(a);
        System.out.println("--------------------------");
    }

    public static void main(String args[]) {
        Pgm2 p2 = new Pgm2();

        p2.lower_triangular_matrix(new int[][]{
                {7,0,0},{48,46,0},{16,28,46}
        });

        p2.lower_triangular_matrix(new int[][]{
                {2,3,4},{5,6,7},{7,6,5}
        });

        p2.lower_triangular_matrix(new int[][]{
                {16, 9, 16, 15, 7, 42, 47},
                {17, 39,14, 28, 48, 4, 1},
                {29, 36, 5, 13, 13, 45, 50},
                {34, 34, 13, 18, 20, 27, 41},
                {37, 23, 16, 24, 32, 35, 19},
                {28, 13, 40, 1, 29, 36, 50},
                {37, 22, 42, 22, 43, 40, 48}
        });
        p2.lower_triangular_matrix(new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        });

        p2.lower_triangular_matrix(new int[][]{
                {7, 47, 17},
                {48, 46, 9},
                {16, 28, 46}
        });

        p2.lower_triangular_matrix(new int[][]{
                {1, 0, 0, 0},
                {5, 6, 0, 0},
                {9, 10, 11, 0},
                {13, 14, 15, 16}
        });
    }
}
