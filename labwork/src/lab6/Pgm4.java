package lab6;

/**
 * Created by kkhan on 26/08/20.
 */

class Car{
    public String tyreBrand;
    private String chassisNo;

    public Car(){
        tyreBrand = "Michelin";
        chassisNo = "12323231212X12323";
    }
}

public class Pgm4 {

    public static void main(String args[]){
        Car car = new Car();
        // Tyre Branch is externally visible and any one can read where chassis No can't be visible to public
        // Line 24 will throw compilation error in case it it uncommented
        System.out.println(car.tyreBrand);
        //System.out.println(car.chassisNo);
    }
}
