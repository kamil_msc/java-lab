package package2;

import package1.PublicVisibility;

/**
 * Created by kkhan on 24/09/20.
 */

class Derived extends PublicVisibility{
    public Derived(){
        System.out.println(protected_variable);
        System.out.println(public_variable);
        // private variable is not visible. If uncommented next line will show compilation errors.
        //System.out.println(private_variable);
    }
}

public class OutsidePackageAccess {
    PublicVisibility publicVisibility = new PublicVisibility();
    // Default visibility class is not accessible across packages. Next line if uncommented will show compilation errors
    //DefaultVisibility dv = new DefaultVisibility();

    public OutsidePackageAccess(){
        // only public visibility variable is visibile
        System.out.println(publicVisibility.public_variable);
        // Default Visibility variable is not visible. uncommenting next line will show compilation error.
        //System.out.println(publicVisibility.default_variable);
        // protected Visibility variable is not visible. uncommenting next line will show compilation error.
        //System.out.println(publicVisibility.protected_variable);

    }
}
