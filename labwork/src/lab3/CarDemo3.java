package lab3;

/**
 * Created by kkhan on 05/08/20.
 */
public class CarDemo3 {
    public static void main(String args[]) {
        Car car_1 = new Car();
        Car car_2 = new Car();

        car_1.calculate_kms_run_with_full_tank_capacity();
        car_2.calculate_kms_run_with_full_tank_capacity();
    }
}
