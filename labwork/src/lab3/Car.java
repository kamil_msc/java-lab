package lab3;

/**
 * Created by kkhan on 05/08/20.
 */
public class Car {
    String color;
    String company;
    String model;
    double fuel_capacity;
    double mileage = 20;


    public Car(){

    }

    public Car(double fc,double m){
        fuel_capacity = fc;
        mileage = m;
    }

    public double calculate_kms_run_with_full_tank_capacity() {
        double d = fuel_capacity * mileage;
        return d;
    }

    public void print_running_distance() {
        double d = fuel_capacity * mileage;
        System.out.println("Kilo metres to run with full tank capacity = " + d);
    }

    public void setFuel_capacity(double fc){
        fuel_capacity = fc;
    }

    public void setMileage(double m){
        mileage = m;
    }
}