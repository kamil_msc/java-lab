package lab3;

public class CarDemo7 {
    public static void main(String args[]) {
        Car car_1 = new Car(20,10);
        Car car_2 = new Car(50,5);
        double kms = car_1.calculate_kms_run_with_full_tank_capacity();
        System.out.println("Kms is " + kms);
        kms = car_2.calculate_kms_run_with_full_tank_capacity();
        System.out.println("Kms is " + kms);
    }
}
