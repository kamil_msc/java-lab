package lab3;

public class CarDemo5 {
    public static void main(String args[]) {
        Car car_1 = new Car();
        Car car_2 = new Car();
        double vol;
        car_1.setFuel_capacity(50);
        car_1.setMileage(10);
        car_2.setFuel_capacity(60);
        car_2.setMileage(8);
        vol = car_1.calculate_kms_run_with_full_tank_capacity();
        System.out.println("Kms is " + vol);
        vol = car_2.calculate_kms_run_with_full_tank_capacity();
        System.out.println("Kms is " + vol);
    }
}
