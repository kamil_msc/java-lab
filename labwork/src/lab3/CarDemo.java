package lab3;

public class CarDemo {
    public static void main(String args[]) {
        Car car = new Car();
        double km_to_run = car.fuel_capacity * car.mileage;
        System.out.println("Kilo metres to run with full tank capacity = " + km_to_run);
    }
}