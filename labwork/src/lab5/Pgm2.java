package lab5;

import util.Input;

/**
 * Created by kkhan on 19/08/20.
 */
public class Pgm2 {


    public static void main(String args[]){

        int r = Input.getIntInput("Enter Row...");
        int c = Input.getIntInput("Enter Col...");
        //int r = 5;
        //int c = 3;
        int matrix[][] = new int[r][c];
        int k = 1;
        for(int i = 0;i<r;i++){
            for(int j = 0;j<c;j++){
                matrix[i][j] = k++;
            }
        }

        for(int i = 0;i<r;i++){
            for(int j = 0;j<c;j++){
                if(j < c-1) {
                    System.out.print(matrix[i][j] + " ");
                }else{
                    System.out.print(matrix[i][j]);
                }
            }
            if(i < r-1){
                System.out.println();
            }
        }

    }
}
