package lab5;

import util.Input;

/**
 * Created by kkhan on 19/08/20.
 */
public class Pgm1 {

    public static boolean sameDigits(String stg){
        //System.out.println("-------------------");
        //System.out.println("Checking " + stg);
        int ones = 0;
        int zeros = 0;
        for(int i = 0;i<stg.length();i++){
            if(stg.charAt(i) == '0')zeros++;
            if(stg.charAt(i) == '1')ones++;
        }
        return ones == 0 || zeros == 0;
    }


    public static void main(String args[]){
        String stg = Input.getInput("Enter the Binary String");

        if(sameDigits(stg)){
            System.out.println("YES");
            return;
        }
        for(int i = 0;i<stg.length();i++){
            String prefix = "";
            if(i > 0) prefix = stg.substring(0,i);
            String suffix = stg.substring(i+1);
            char c = stg.charAt(i);
            if(c == '0') {
                if (sameDigits(prefix + "1" + suffix)) {
                    System.out.println("YES");
                    return;
                }
            }
            if(c == '1') {
                if (sameDigits(prefix + "0" + suffix)) {
                    System.out.println("YES");
                    return;
                }
            }
        }
        System.out.println("NO");
    }
}
