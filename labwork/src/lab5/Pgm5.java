package lab5;

/**
 * Created by kkhan on 19/08/20.
 */
public class Pgm5 {

    public static int[] copy(int src[]){
        int[] dst = new int[src.length];
        for(int i = 0;i<src.length;i++){
            dst[i] = src[i];
        }
        return dst;
    }

    public static void main(String args[]){
        int[] s= new int[]{4,5,6,12};
        int[] d = copy(s);
        for(int i = 0;i<s.length;i++){
            System.out.println(s[i] + "  " + d[i]);
        }
    }
}
