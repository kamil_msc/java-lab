import java.io.*;
import java.lang.reflect.*;

/**
 * Created by kkhan on 22/07/20.
 */
public class Lab1 {

    private void ex1(BufferedReader reader) throws IOException{
        System.out.println("Enter a...");
        Integer a = Integer.parseInt(reader.readLine());
        System.out.println("Enter b...");
        Integer b = Integer.parseInt(reader.readLine());
        System.out.println(a + b);
    }

    private void ex2(BufferedReader reader) throws IOException{
        System.out.println("Enter a...");
        Integer a = Integer.parseInt(reader.readLine());
        System.out.println("Enter b...");
        Integer b = Integer.parseInt(reader.readLine());
        if(a > b){
            System.out.println(a + " is bigger");
        }else{
            System.out.println(b + " is bigger");
        }
    }

    private void ex9(BufferedReader reader) throws IOException{
        System.out.println("Enter a...");
        Integer a = Integer.parseInt(reader.readLine());
        System.out.println("Enter b...");
        Integer b = Integer.parseInt(reader.readLine());
        System.out.println("Enter c...");
        Integer c = Integer.parseInt(reader.readLine());
        if(a < b){
            if(a < c) {
                System.out.println(a + " is smallest");
            }else{
                System.out.println(c + " is smallest");
            }
        }else{
            if(b < c) {
                System.out.println(b + " is smallest");
            }else{
                System.out.println(c + " is smallest");
            }
        }
    }

    private void ex3(BufferedReader reader) throws IOException{
        System.out.println("Enter No...");
        Integer no = Integer.parseInt(reader.readLine());;
        if(no %2 == 1){
            System.out.println("Odd");
        }else{
            System.out.println("Even");
        }
    }

    private void ex4(BufferedReader reader) throws IOException{
        System.out.println("Enter Year...");
        Integer no = Integer.parseInt(reader.readLine());;
        if(no %400 == 0){
            System.out.println("Leap Year");
        }else if(no %4 == 0 && no %100 != 0){
            System.out.println("Leap Year");
        }else{
            System.out.println("Not a Leap Year");
        }
    }

    private void ex5(BufferedReader reader) throws IOException{
        System.out.println("Enter Vowel...");
        Character c = reader.readLine().charAt(0);
        if(c == 'a') System.out.println("Apple");
        if(c == 'e') System.out.println("Egg");
        if(c == 'i') System.out.println("Ink");
        if(c == 'o') System.out.println("Owl");
        if(c == 'u') System.out.println("Umberlla");
    }

    private void ex6(BufferedReader reader) throws IOException {
        System.out.println("Enter Vowel...");
        Character c = reader.readLine().charAt(0);
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') System.out.println("Vowel");
        else
            System.out.println("Consonant");
    }

    private void ex7(BufferedReader reader) throws IOException {
        System.out.println("Welcome to Java Programming");
    }

    private void ex8(BufferedReader reader) throws IOException {
        System.out.println("Integer " + Integer.MIN_VALUE + " to " + Integer.MAX_VALUE);
        System.out.println("Double " + Double.MIN_VALUE + " to " + Double.MAX_VALUE);
        System.out.println("Character " + Character.MIN_VALUE + " to " + Character.MAX_VALUE);
    }

    private void ex10(BufferedReader reader) throws IOException{
        System.out.println("Enter a...");
        Integer a = Integer.parseInt(reader.readLine());
        System.out.println("Enter b...");
        Integer b = Integer.parseInt(reader.readLine());
        System.out.println("Quotient " + a / b);
        System.out.println("Remainder " + a % b);
    }


    public static void main(String args[]){
        Lab1 lab1 = new Lab1();
        while(true){
            System.out.println("1-Sum of 2 nos 2-Biggest of 2 nos 3-Odd or Even 4-Leap Year 5-Word for Vowel" +
                    " 6-Check character is vowel 7-Welcome Message 8-Value of Integers,Double and String " +
                    "9-Smallest of Three Numbers 10-Quotient and Remainder 11-Quit");
            try {
                Method methods[] = lab1.getClass().getDeclaredMethods();
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                Integer no = Integer.parseInt(reader.readLine());
                if(no == 11){
                    System.exit(1);
                }
                Method method = lab1.getClass().getDeclaredMethod("ex"+no , new Class[]{BufferedReader.class});
                method.setAccessible(true);
                method.invoke(lab1,reader);


            }catch(IOException ioe){
                ioe.printStackTrace();
            }catch(NumberFormatException pe){
                pe.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }


    }
}
