package lab7;

import util.Input;

/**
 * Created by kkhan on 19/08/20.
 */
public class Pgm1 {

    public boolean isBinaryMatrix(int[][] matrix){
        for(int i = 0;i<matrix.length;i++){
            for(int j = 0;j<matrix[i].length;j++){
                if(matrix[i][j] != 1 && matrix[i][j] != 0){
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String args[]){
        Pgm1 pgm1 = new Pgm1();
        System.out.println(pgm1.isBinaryMatrix(new int[][]{{1,2},{1,0}}));
        System.out.println(pgm1.isBinaryMatrix(new int[][]{{1,1},{1,0}}));
    }
}
