package lab7;


class Reverse{

    public int[] reverse(int[] a){
        int r[] = new int[a.length];
        for(int i = 0;i<a.length;i++){
            r[a.length-1-i] = a[i];
        }
        return r;
    }

}

class Sum{
    public int[] sum(int[] a,int[] b){
        int r[] = new int[a.length];
        for(int i = 0;i<a.length;i++){
            r[i] = a[i] + b[i];
        }
        return r;
    }
}

/**
 * Created by kkhan on 02/09/20.
 */
public class Pgm5 {

    Reverse reverse = new Reverse();
    Sum sum = new Sum();

    private void print(int r[]){
        for(int i = 0;i<r.length;i++){
            System.out.print(r[i] + "  ");
        }
        System.out.println();
    }
    public void test(int a[]){
        print(a);
        int[] r = reverse.reverse(a);
        print(r);
        print(sum.sum(a,r));
        System.out.println("------------------");
    }
    public  static  void main(String args[]){
        Pgm5 pgm5 = new Pgm5();
        pgm5.test(new int[]{2,5,3,1});
        pgm5.test(new int[]{9,9,1,9,3,2});
        pgm5.test(new int[]{4,3,2,1,3});
        pgm5.test(new int[]{1});
        pgm5.test(new int[]{1,1,11,1,1,1,1,1});

    }
}
