package lab7;

/**
 * Created by kkhan on 02/09/20.
 */
public class Pgm3 {

    public String fila(String s){
        if(s.length() < 2){
            return "";
        }
        return s.substring(0,2) + s.substring(s.length()-2,s.length());
    }

    public static void main(String args[]) {
        Pgm3 pgm3 = new Pgm3();
        String t[] = new String[]{"k", "hh", "String", "PYTHON", "Magic Square", "Matrix"};
        for (String s : t) {
            System.out.println(s + " = " + pgm3.fila(s));
        }
    }
}
