package lab7;
import java.util.*;

//Class
class Referee{

    // static Variable. Referee can book players of any team
    static List<String> bookedPlayers;

    // static Block. Init the booked players list
    static{
        bookedPlayers = new ArrayList();
    }

    // static method. Referee can book any playing player
    public static void bookPlayer(String playerName){
        bookedPlayers.add(playerName);
    }
}

class Team{

    // final variable. Can be set only once (in this case in constructor). Captain of team cannot be changed during the game
    final String captain;
    List<String> players = new ArrayList<String>();
    List<String> reservedPlayers = new ArrayList<String>();;

    public void changePlayer(String e,String n){
        if(reservedPlayers.contains(n)){
            players.remove(e);
            players.add(n);
            reservedPlayers.remove(n);
            reservedPlayers.add(e);
        }
    }


    public Team(String captain,String[] players,String[] reservedPlayers){
        this.captain = captain;
        for(int i = 0;i<players.length;i++)
            this.players.add(players[i]);
        for(int i = 0;i<reservedPlayers.length;i++)
            this.reservedPlayers.add(reservedPlayers[i]);
    }
}

/**
 * Created by kkhan on 02/09/20.
 */
public class Pgm4 {

    // teamA, teamB are objects of class Team
    public static void main(String args[]){
        Team teamA = new Team("A", new String[]{"A","B","C","F"},new String[]{"D,E,G"});
        Team teamB = new Team("X", new String[]{"X","Y","Z","W"},new String[]{"P,Q,R"});
        teamA.changePlayer("B","D");
        Referee.bookPlayer("Y");

    }

}
