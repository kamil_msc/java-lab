package lab7;

/**
 * Created by kkhan on 02/09/20.
 */
public class Pgm2 {

    public String usingBuiltin(String s){
        String r = "";
        for(int i = 0;i<s.length();i++){
            char c = s.charAt(i);
            if(Character.isUpperCase(c)){
                r = r + Character.toLowerCase(c);
            }else if(Character.isLowerCase(c)){
                r = r + Character.toUpperCase(c);
            }else{
                r = r + c;
            }
        }
        return r;
    }

    public String usingASCIICode(String s){
        String r = "";
        for(int i = 0;i<s.length();i++){
            int c = s.charAt(i);
            if(c >= 65 && c<= 91){
                c = c + 32;
            }else if(c >= 97 && c<= 123){
                c = c - 32;
            }
            r = r + (char)c;
        }
        return r;
    }

    public static void main(String args[]){
        Pgm2 pgm2 = new Pgm2();

        String msg = "COVID-19 is not fatal";
        System.out.println(pgm2.usingASCIICode(msg));
        System.out.println(pgm2.usingBuiltin(msg));

        msg = "Hello World";
        System.out.println(pgm2.usingASCIICode(msg));
        System.out.println(pgm2.usingBuiltin(msg));
    }
}
