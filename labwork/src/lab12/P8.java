package lab12;
import java.net.*;

/**
 * Created by kkhan on 07/10/20.
 */
public class P8 {
    public static void main(String args[]){
        // Unless Malformed URL Exception is handled, this code will not compile. This is checked exception.
        try{
            URL url = new URL("http://google.com");
        }catch(MalformedURLException mue){

        }
        // Divide by zero will result in Run time. But compile time, this exception is not raised. This is unchecked exception.
        int a = 12/0;
    }
}
