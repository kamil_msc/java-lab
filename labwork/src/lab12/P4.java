package lab12;

import java.io.*;

/**
 * Created by kkhan on 07/10/20.
 */
public class P4 {
    public static void main(String args[]){
        try{

            BufferedReader  reader = new BufferedReader(new FileReader(new File("a.txt")));
        }catch(FileNotFoundException fne){
            System.out.println("File is not present");
        }catch(IOException ioe){
            System.out.println("Unable to read. Check File Read Permission");

        }
    }
}
