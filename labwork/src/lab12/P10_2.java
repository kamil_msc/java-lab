package lab12;

class TestE extends Exception { }
class P10_2 {
    public static void main(String args[]) {
        try {
            throw new TestE();
        }
        catch(TestE t) {
            System.out.println("Got the Test Exception");

        }
        finally {
            System.out.println("Inside finally  block ");

        }
    }
}