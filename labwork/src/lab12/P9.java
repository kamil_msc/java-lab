package lab12;

class NegativeNumberException extends Exception{
    public NegativeNumberException(int p){
        super("Natural Number cannot be negative. Check the passed value " + p);
    }
}

/**
 * Created by kkhan on 07/10/20.
 */
public class P9 {

    private static void operateOnNaturalNumbers(int n) throws NegativeNumberException{
        if(n <= 0){
            throw new NegativeNumberException(n);
        }
        System.out.println(n + " can be operated upon");
    }

    public static void main(String args[]) throws NegativeNumberException{
        P9.operateOnNaturalNumbers(12);
        P9.operateOnNaturalNumbers(-12);
    }
}
