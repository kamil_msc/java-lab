package lab12;

import java.net.*;

/**
 * Created by kkhan on 07/10/20.
 */
public class P6 {
    // by throwing the exception to the caller, checked and
    // unchecked exceptions need not be handled in the concerned code and it needs to be tacked by the caller.
    public static void main(String args[]) throws MalformedURLException{
        URL url = new URL("google.com");
    }
}
