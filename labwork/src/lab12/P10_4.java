package lab12;

/**
 * Created by kkhan on 07/10/20.
 */
class P10_4
{
    String str = "a";
    void A(){
        try
        {
            str +="b";
            B();
        }
        catch (Exception e)
        {
            str += "c";
        }
    }
    void B() throws Exception
    {
        try
        {
            str += "d";
            C();
        }
        catch(Exception e)
        {
            throw new Exception();
        }
        finally
        {
            str += "e";
        }
        str += "f";
    }
    void C() throws Exception
    {
        throw new Exception();
    }
    void display()
    {
        System.out.println(str);
    }
    public static void main(String[] args)
    {
        P10_4 object = new P10_4();
        object.A();
        object.display();
    }
}