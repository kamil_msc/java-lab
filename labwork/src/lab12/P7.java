package lab12;


import java.io.*;

/**
 * Created by kkhan on 07/10/20.
 */
public class P7 {
    public static void main(String args[]){
        try{
            BufferedReader pw = new BufferedReader(new InputStreamReader(new FileInputStream(new File("a.txt"))));
            pw.readLine();
            pw.close();
        }catch(IOException e){
            System.out.println("File is not present");
        }finally{
            System.out.println("Whether File is there or not, I am executed since i am under finally block");
        }
    }
}
