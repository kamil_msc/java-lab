package linet;

/**
 * Created by kkhan on 19/08/20.
 */
public class Code1 {
    public static void main(String args[]){
        int a[] = new int[]{ 2,3,1,4 };
        int[] b = new int[a.length];
        System.out.println();
        System.out.println("Array is:");
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i] + " ");
        }
        System.out.println();
        System.out.println("Array in reverse order: ");
        //Loop through the array in reverse order
        for (int i = a.length-1; i >= 0; i--) {

            //b[i] = a[i];
            b[i] = a[a.length-i-1];
            System.out.print(b[i]+ " ");
        }
        System.out.println();
        int[] sum = new int[a.length];
        for(int i = 0;i<a.length;i++){
            sum[i] =a[i] + b[i];
            System.out.println(a[i] + "  " + b[i] +" " + sum[i]);
        }
    }
}
