package lab9;

class A9 {
    public int i;
    public int j;

    A9() {
        i = 1;
        j = 2;
    }
}

class B9 extends A9 {
    int a;

    B9() {
        super();
    }
}

class P9 {
    public static void main(String args[]) {
        B9 obj = new B9();
        System.out.println(obj.i + " " + obj.j);
    }
}

// Output : 1 2