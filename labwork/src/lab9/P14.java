package lab9;

class X
{
    public X(int i)
    {
        System.out.println(1);
    }
}

class Y extends X
{
    public Y()
    {
        super(0);
        System.out.println(2);
    }
}

// Call the Super class