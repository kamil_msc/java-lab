package lab9;
import java.awt.Shape;

/*
A public class Circle implements Shape
{
    private int radius;
}

B public abstract class Circle extends Shape
{
    private int radius;

}
C public class Circle extends Shape
{
    private int radius;
    public void draw();
}


D public abstract class Circle implements Shape
{
    private int radius;
    public void draw();
}

E public class Circle extends Shape
{
    private int radius;
    public void draw()
    {
    }
}


F public abstract class Circle implements Shape  {
    private int radius;
    public void draw()
    {
    }
}
*/
// Ans: