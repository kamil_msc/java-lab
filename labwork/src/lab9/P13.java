package lab9;

class X13
{
    static void staticMethod()
    {
        System.out.println("Class X");
    }
}

class Y13 extends X13
{
    static void staticMethod()
    {
        System.out.println("Class Y");
    }
}

public class P13
{
    public static void main(String[] args)
    {
        Y13.staticMethod();
    }
}

// Ans: Class Y. because static method has nothing to do with inheritance