package lab9;

class BaseP3 {
    public static void show() {
        System.out.println("Base::show() called");

    }
}

class DerivedP3 extends BaseP3 {
    public static void show() {
        System.out.println("Derived::show() called");

    }
}

class P3 {
    public static void main(String[] args) {
        Base b = new Derived();;
        b.show();
    }
}
