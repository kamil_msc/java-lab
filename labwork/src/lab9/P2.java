package lab9;

class BaseP2 {
    final public void show() {
        System.out.println("Base::show() called");

    }
}

class DerivedP2 extends BaseP2 {
    public void showe() {
        System.out.println("Derived::show() called");

    }
}

class P2 {
    public static void main(String[] args) {
        Base b = new Derived();;
        b.show();
    }
}

// Ans: Compiler Error. final method can't be overloaded.