package lab9;

class Base {
    public void show() {
        System.out.println("Base::show() called");
    }
}
class Derived extends Base {
    public void show() {
        System.out.println("Derived::show() called");
    }
}

public class P1 {
    public static void main(String[] args) {
        Base b = new Derived();;
        b.show();
    }
}

// Answer : A - Derived:show()  called. Because the derived object is created.