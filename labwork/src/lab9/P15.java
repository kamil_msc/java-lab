package lab9;

class M {
    static {
        System.out.println('A');
    }

    {
        System.out.println('B');
    }

    public M() {
        System.out.println('C');
    }
}

class N extends M {
    static {
        System.out.println('D');
    }

    {
        System.out.println('E');
    }

    public N() {
        System.out.println('F');
    }
}

public class P15 {
    public static void main(String[] args) {
        N n = new N();
    }
}

/*
Output:
A
D
B
C
E
F

static blocks of the base classes followed by derived classes are executed first
instantiation blocks of the base classes followed by derived classes next.
Base class constructors and followed by derived class constructors
 */