package lab9;

class BaseP4 {
    public void Print() {
        System.out.println("Base");
    }
}

class DerivedP4 extends BaseP4 {
    public void Print() {
        System.out.println("Derived");
    }
}

class P4{
    public static void DoPrint( BaseP4 o ) {
        o.Print();
    }
    public static void main(String[] args) {
        BaseP4 x = new BaseP4();
        BaseP4 y = new DerivedP4();
        DerivedP4 z = new DerivedP4();
        DoPrint(x);
        DoPrint(y);
        DoPrint(z);
    }
}

/* Ans:
Base
Derived
Derived
 */
