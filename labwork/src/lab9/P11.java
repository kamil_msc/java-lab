package lab9;


class A11
{
    static
    {
        System.out.println("THIRD");
    }
}

class B11 extends A11
{

    static
    {
        System.out.println("SECOND");
    }
}

class C11 extends B11
{
    static
    {
        System.out.println("FIRST");
    }
}

public class P11
{
    public static void main(String[] args)
    {
        C11 c = new C11();
    }
}

// Ans: THIRD SECOND FIRST
// static functions are called before the object is created