package lab9;

class A12
{
    public A12()
    {
        System.out.println("Class A Constructor");
    }
}

class B12 extends A12
{
    public B12()
    {
        System.out.println("Class B Constructor");
    }
}

class C12 extends B12
{
    public C12(){
        System.out.println("Class C Constructor");
    }
}

public class P12
{
    public static void main(String[] args)
    {
        C12 c = new C12();
    }
}


/* Output

Class A Constructor
Class B Constructor
Class C Constructor

base class is instantiated before derived classes.
 */