package lab9;

class A8 {
    int i;
}

class B8 extends A8 {
    int j;

    void display() {
        super.i = j + 1;
        System.out.println(j + " " + i);
    }
}

class P8 {
    public static void main(String args[]) {
        B8 obj = new B8();
        obj.i = 1;
        obj.j = 2;
        obj.display();
    }
}

// Ans: 2 3