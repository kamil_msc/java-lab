package lab9;

class A7 {
    int i;

    void display() {
        System.out.println(i);
    }
}

class B7 extends A7 {
    int j;

    void display() {
        System.out.println(j);
    }
}

class P7 {
    public static void main(String args[]) {
        B7 obj = new B7();
        obj.i = 1;
        obj.j = 2;
        obj.display();
    }
}

// Output: 2 (Derived class is instantiated. So display of B is called
