package lab9;

class BaseP5 {
    public void foo() {
        System.out.println("Base"); }
}

class DerivedP5 extends BaseP5 {
    public void foo() {
        System.out.println("Derived"); }
}

public class P5 {
    public static void main(String args[]) {
        BaseP5 b = new DerivedP5();
        b.foo();
    }
}

// Ans: Compilation error. private access specifier is not allowed. Visibility of base class function can't be reduced in derived class
