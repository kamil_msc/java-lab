package lab4;

/**
 * Created by kkhan on 12/08/20.
 */
public class Pblm1 {

    public static  void main(String args[]){
        System.out.println("Original Array");
        for(int i = 0; i< args.length;i++) {
            System.out.print(args[i] + " ");
        }
        System.out.println("\n-----------------");
        System.out.println("\nResultant Array");
        for(int i = 0; i< args.length;i++){
            int t = Integer.parseInt(args[i]) + Integer.parseInt(args[args.length-i-1]);
            System.out.print(t + "  ");
        }
    }
}
