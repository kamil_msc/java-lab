package lab4;

import java.util.Arrays;

/**
 * Created by kkhan on 12/08/20.
 */
public class Pblm2 {

    private static boolean present(int array[],int value){
        for(int i = 0;i<array.length;i++){
            if(array[i] == value) return true;
        }
        return false;
    }

    private static boolean condition(int k_array[],int min_max,int m,int v){
        if(min_max == 1) {
            return v > m && !present(k_array, v);
        }
        else {
            return v < m && !present(k_array, v);
        }
    }

    private static int findMinMax(int a[],int k,int min_max){
        int[] k_array = new int[k];
        for(int iteration = 0;iteration<k;iteration++){
            int m = Integer.MIN_VALUE;
            if(min_max == -1) m = Integer.MAX_VALUE;
            for(int i = 0;i<a.length;i++){
                if(condition(k_array,min_max,m,a[i])){
                    m = a[i];
                }
            }
            k_array[iteration] = m;
        }
        return k_array[k-1];
    }

    public static void main(String args[]){
        int a[] = { 12,34,45,56,74,1,23,8,88 };
        System.out.println(findMinMax(a,3,1));
        System.out.println(findMinMax(a,4,-1));
        /*
        System.out.println("Sort to check the answer ...");
        Arrays.sort(a);
        for(int i = 0;i<a.length;i++){
            System.out.print(i + " ");
        }
        System.out.println("");
        for(int i = 0;i<a.length;i++){
            System.out.print(a[i] + " ");
        }
        */

    }
}
