package lab15;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by kkhan on 28/10/20.
 */
public class P1 {

    public static void main(String args[]){
        Frame frame = new Frame();
        frame.setSize(200,200);
        frame.add(new Label("First Frame"));
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

            }

            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(1);

            }

            @Override
            public void windowIconified(WindowEvent e) {


            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        frame.show();
    }
}
