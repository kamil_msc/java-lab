package lab15;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kkhan on 28/10/20.
 */
public class P3 extends Frame {

    public P3(){
        setSize(500,500);
        setLayout(new GridLayout(10,1));
        add(new Label("I am a label..."));
        TextField tf = new TextField();
        tf.setText("I am a text field. Enter Ur Value...");
        add(tf);
        Button button = new Button("I am a Button to Close the frame");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        add(button);
    }
    public static void main(String args[]){
        P3 p3 = new P3();
        p3.show();
    }
}
