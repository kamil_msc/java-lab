package lab15;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


class XCanvas extends Canvas{
    public void paint(Graphics g){
        // set color to red
        g.setColor(Color.red);

        // set Font
        g.setFont(new Font("Bold", 1, 20));
        g.drawString("AWT Canvas",30,30);
    }
}
/**
 * Created by kkhan on 28/10/20.
 */
public class P5 extends Frame {

    public P5() {
        setSize(500, 500);
        setLayout(new GridLayout(10, 1));
        List l1=new List(5);
        l1.setBounds(100,100, 75,75);
        l1.add("Item 1");
        l1.add("Item 2");
        l1.add("Item 3");
        l1.add("Item 4");
        l1.add("Item 5");

        Choice c=new Choice();
        c.setBounds(100,100, 75,75);
        c.add("C 1");
        c.add("C 2");
        c.add("C 3");
        c.add("C 4");
        c.add("C 5");
        add(c);
        add(l1);
        XCanvas canvas = new XCanvas();
        canvas.setBackground(Color.black);
        add(canvas);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });
    }
    public static void main(String args[]){
        P5 p5 = new P5();
        p5.show();
    }
}
