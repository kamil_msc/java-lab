package lab15;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kkhan on 28/10/20.
 */
public class P2 extends Frame {

    public P2(){
        setSize(200,200);
        Button button = new Button("Close the frame");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        add(button);
    }
    public static void main(String args[]){
        P2 p2 = new P2();
        p2.show();
    }
}
