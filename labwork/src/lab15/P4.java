package lab15;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by kkhan on 28/10/20.
 */
public class P4 extends Frame {

    public P4() {
        setSize(500, 500);
        setLayout(new GridLayout(10, 1));
        TextArea ta = new TextArea();
        ta.setText("I am a text area. Enter Ur Value...");
        add(ta);

        Checkbox cb = new Checkbox();
        add(cb);
        cb.setLabel("Single");

        Checkbox red = new Checkbox();
        add(red);
        red.setLabel("Red");
        Checkbox green = new Checkbox();
        add(green);
        green.setLabel("Green");
        Checkbox blue = new Checkbox();
        add(blue);
        blue.setLabel("Blue");

        CheckboxGroup cbg = new CheckboxGroup();
        red.setCheckboxGroup(cbg);
        blue.setCheckboxGroup(cbg);
        green.setCheckboxGroup(cbg);


        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });
    }
    public static void main(String args[]){
        P4 p4 = new P4();
        p4.show();
    }
}
