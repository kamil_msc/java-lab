package lab15;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 * Created by kkhan on 28/10/20.
 */
public class P6 extends Frame {

    public P6() {
        setSize(500, 500);
        setLayout(new GridLayout(10, 2));
        add(new Label("Enter Roll No"));
        add(new TextField());
        add(new Label("Enter Name"));
        add(new TextField());

        add(new Label("Choose Subjects"));
        List l1=new List(5);
        l1.setBounds(100,100, 75,75);
        l1.add("Advanced Java");
        l1.add("Data Science");
        l1.add("Cryptography");
        l1.add("IOT");
        l1.add("Network Security");
        l1.add("Software Testing");
        l1.setMultipleSelections(true);
        add(l1);

        add(new Label("Total No of Subjects = 6"));
        add(new Label("Total Fees = 500"));
        add(new Button("Submit"));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });


    }
    public static void main(String args[]){
        P6 p6 = new P6();
        p6.show();
    }
}
